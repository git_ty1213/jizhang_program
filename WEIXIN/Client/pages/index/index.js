var util = require('../../utils/util.js');
var imageUtil = require('../../utils/imageutil.js');
import utils from '../../utils/index'
var app = getApp()
var budgetmoney;
Page({

  /**
   * 页面的初始数据
   */
  data: {
    scrollTop: 0,
    items: [],
    expensetotal: 0,
    incometotal: 0,
    tableID: app.globalData.tableId,
    imagefirstsrc: 'https://gitee.com/git_ty1213/jizhang_program/raw/45ca4349ccfab9a21b1e789bfdddbf5d1c125cc7/WEIXIN/Client/images/index.jpeg',

    currentTab: '',
    budget: '',
    showModal: false,

  },

  budgetBtn: function () {
    if (util.formatTime(new Date).substring(8, 10) == '01') {
      wx.showModal({
        title: '提示',
        content: '每月1号不能设置预算！\r\n客官请明天再来！',
        showCancel: false,
        success: function () {
        }
      });
    }//每月1号，不能设置预算
    else {
      this.setData({
        currentTab: 1,
        showModal: true
      })
    }
  },
  inputBudget: function (e) {
    this.setData({
      budget: e.detail.value,
    })
  },


  hideModal: function () {
    this.setData({
      showModal: false
    });
  },
  /**
   * 对话框保存按钮点击事件
   */
  onSave: function () {

    this.hideModal();
    if (this.data.budget != '') {//输入不为空
      if (parseFloat(this.data.expensetotal) >= parseFloat(this.data.budget) * 0.8 && parseFloat(this.data.expensetotal) < parseFloat(this.data.budget)) {//超80%
        wx.setStorageSync('key', this.data.budget);
        this.setData({
          currentTab: 1
        })
        wx.showModal({
          title: '提示',
          content: '您本月支出已达预算80%！\r\n客官省着点用啦！',
          showCancel: false,
          success: function () {
          }
        });
      }//超过80%，显示，提示
      else if (parseFloat(this.data.expensetotal) >= parseFloat(this.data.budget)) {//支出大于预算
        wx.setStorageSync('key', '');
        this.setData({
          currentTab: 0,
          budget: ''
        })
        wx.showModal({
          title: '提示',
          content: '您设置的预算低于支出！\r\n客官重新设置预算吧！',
          showCancel: false,
          success: function () {
          }
        });
      }//大于预算，制空，不显示，提示
      else {
        wx.setStorageSync('key', this.data.budget);
        this.setData({
          currentTab: 1
        })
      }//存预算，显示
    }
    else {//输入为空
      this.setData({
        currentTab: 0
      })
      wx.showModal({
        title: '提示',
        content: '您未输入预算金额！\r\n客官重新设置预算吧！',
        showCancel: false,
        success: function () {
        }
      });
    }

  },
  /**
   * 对话框删除按钮点击事件
   */
  onDelete: function () {
    this.hideModal();
    wx.setStorageSync('key', '');
    this.setData({
      currentTab: '0',
    })
  },
  onShow: function () {
    this.fetchItems();
    // console.log(util.formatTime(new Date));
  },

  fetchItems() {
    utils.getBills(this, (res) => {
      this.setData({
        items: res.data.objects // bookList array, mock data in mock/mock.js
      })
      this.calTotal(res.data.objects);
      //console.log(res.data.objects);
    });


    if (wx.getStorageSync('key') != '') {

      if (util.formatTime(new Date).substring(8, 10) == '01') {
        this.setData({
          currentTab: 0,
        })
        wx.setStorageSync('key', '');
        wx.showModal({
          title: '提示',
          content: '每月1号不能设置预算！\r\n客官请明天再来！',
          showCancel: false,
          success: function () {
          }
        });
      }//如果值不为空，且是每月1号，预算清零
      else {//不是1号
        this.setData({
          currentTab: 1,
          budget: wx.getStorageSync('key')
        })//显示预算
        if (parseFloat(this.data.expensetotal) >= parseFloat(this.data.budget)) {//支出大于预算

          wx.showModal({
            title: '提示',
            content: '您支出已超过预算！\r\n客官重新设置预算吧！',
            showCancel: false,
            success: function () {
            }
          });
        }
        if (parseFloat(this.data.expensetotal) >= parseFloat(this.data.budget) * 0.8 && parseFloat(this.data.expensetotal) < parseFloat(this.data.budget)) {//超80%
          wx.showModal({
            title: '提示',
            content: '您本月支出已达预算80%！\r\n客官省着点用啦！',
            showCancel: false,
            success: function () {
            }
          });
        }//超过80%，显示，提示
        else { }

      }//不是1号
    }//预算有值
    else {
      this.setData({
        currentTab: 0,
      })
    }//值为空，不显示预算
  },

  preventTouchMove: function () {
  },

  calTotal: function (data) {
    var tempTotal1 = 0; //存放支出总金额
    var tempTotal2 = 0; //存放收入总金额
    var time = util.formatTime(new Date);
    var subtime = '';
    subtime = time.substring(0, 7);
    console.log(subtime);
    for (var x in data) {
      if (data[x].billCate == '-') {
        //如果是支出，则判断支出日期是否匹配当月日期
        if (subtime == data[x].billDate.substring(0, 7)) {
          tempTotal1 += parseFloat(data[x].billMoney);
        }
      }
      if (data[x].billCate == '+') {
        //如果是收入，则判断收入日期是否匹配当月日期
        if (subtime == data[x].billDate.substring(0, 7)) {
          tempTotal2 += parseFloat(data[x].billMoney);
        }
      }
    }
    this.setData({
      expensetotal: tempTotal1,
      incometotal: tempTotal2
    });
  },

  edit: function (e) {
    //带参数跳转页面
    wx.navigateTo({
      url: '../edit/edit?index=' + e.currentTarget.dataset.index + '&Type=' + e.currentTarget.dataset.name + '&money=' + e.currentTarget.dataset.money + '&cate=' + e.currentTarget.dataset.cate + '&date=' + e.currentTarget.dataset.date + '&remarks=' + e.currentTarget.dataset.remarks + '&id=' + e.currentTarget.dataset.id
    });

  },
  onShareAppMessage: function () {
    return {
      title: '账单分享',
      path: '/pages/index/index',
      success: function (res) {
       // console.log(res)
      }
    }
  },
  onPullDownRefresh: function () {
    wx.stopPullDownRefresh()
  },
  imageLoad: function (e) {
    var imageSize = imageUtil.imageUtil(e)
    this.setData({
      imagewidth: imageSize.imageWidth,
      imageheight: imageSize.imageHeight
    })
  }

})