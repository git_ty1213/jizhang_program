var imageUtil = require('../../utils/imageutil.js');
var util = require('../../utils/util.js');
var utils = require('../../utils/index.js')
Page({

  /**
   * 页面的初始数据
   */
  data: {

    id: null,

    money: '',
    remarks: '',
    index: '',
    date: '',
    Type: '',
    cate: '',
    selectType: true,
    selectArea: false,

    temp: [],

    modalHidden_save: true,
    modalHidden_delete: true,

    imagefirstsrc: 'https://gitee.com/git_ty1213/jizhang_program/raw/953c758bd61c42a287cbba5dd2734324b346f44b/WEIXIN/Client/images/income.jpeg',
    imagetype1: 'https://gitee.com/git_ty1213/jizhang_program/raw/d16d9dadd8d72a1a99bd0dda9c853c3b2bfec344/WEIXIN/Client/images/type1.png',
    imagetype2: 'https://gitee.com/git_ty1213/jizhang_program/raw/d16d9dadd8d72a1a99bd0dda9c853c3b2bfec344/WEIXIN/Client/images/type2.png',
    imagetype3: 'https://gitee.com/git_ty1213/jizhang_program/raw/d16d9dadd8d72a1a99bd0dda9c853c3b2bfec344/WEIXIN/Client/images/type3.png',
    imagesavebtn: 'https://gitee.com/git_ty1213/jizhang_program/raw/d16d9dadd8d72a1a99bd0dda9c853c3b2bfec344/WEIXIN/Client/images/savebtn.png',
  },
  //点击选择类型
  clickType: function () {
    var selectType = this.data.selectType;
    if (selectType == true) {
      this.setData({
        selectArea: true,
        selectType: false,
      })
    } else {
      this.setData({
        selectArea: false,
        selectType: true,
      })
    }
  },
  //点击切换
  mySelect: function (e) {
    this.setData({
      Type: e.currentTarget.dataset.mtype,
      selectType: true,
      selectArea: false,
    })
  },
  moneyInput: function (e) {
    this.setData({
      money: e.detail.value
    })
  },
  bindDateChange: function (e) {
    var that = this;
    that.setData({
      date: e.detail.value
    })
    //console.log(that.data.date);
  },
  remarksInput: function (e) {
    this.setData({
      remarks: e.detail.value
    })
  },
  delBtnClick: function () {

    utils.deleteBill(this, (res) => {
      this.setData({
        modalHidden_delete: false
      })
    })
  },
  saveBtnClick: function (e) {
    utils.updateBill(this, (res) => {
      this.setData({
        modalHidden_save: false
      })
    })


  },

  hideModal: function () {
    this.setData({
      Type: '',
      date: '',
      money: '',
      remarks: ''
    });
    wx.switchTab({
      url: '../index/index',
    });
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (e) {
    this.setData({
      index: e.index,
      id: e.id,
      cate: e.cate,
    })

    //console.log("id测试" + this.data.id)

    var cate = this.data.cate

    if (cate == '-') {
      this.setData({
        Type: e.Type,
        money: e.money,
        date: e.date,
        remarks: e.remarks
      })
    } else if (cate == '+') {
      this.setData({
        Type: e.Type,
        money: e.money,
        date: e.date,
        remarks: e.remarks
      })
    }

  },
  onPullDownRefresh: function () {
    wx.stopPullDownRefresh()
  },
  imageLoad: function (e) {
    var imageSize = imageUtil.imageUtil(e)
    this.setData({
      imagewidth: imageSize.imageWidth,
      imageheight: imageSize.imageHeight
    })
  },
  
})