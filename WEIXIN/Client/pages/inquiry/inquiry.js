var util = require('../../utils/util.js')
var imageUtil = require('../../utils/imageutil.js');
import utils from '../../utils/index'
var app = getApp()
Page({

  /**
   * 页面的初始数据
   */
  data: {
    winWidth: 0,
    winHeight: 0,
    // tab切换  
    currentTab: '3',
    items: [],
    expenseTotal: 0,
    incomeTotal: 0,
    tableID: app.globalData.tableId,
    selectDay: '',
    selectMonth: '',
    selectYear: '',

    nowdate: util.formatTime(new Date),
    scrollTop: 0,
    day: [],
    month: [],
    year: [],
    imagefirstsrc: 'https://gitee.com/git_ty1213/jizhang_program/raw/45ca4349ccfab9a21b1e789bfdddbf5d1c125cc7/WEIXIN/Client/images/inquiry.jpeg'
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    var that = this;
    wx.getSystemInfo({
      success: function (res) {
        that.setData({
          winWidth: res.windowWidth,
          winHeight: res.windowHeight
        });
      }
    });
  },
  bindChange: function (e) {
    var that = this;
    that.setData({ currentTab: e.detail.current });
  },
  swichNav: function (e) {
    var that = this;
    if (this.data.currentTab === e.target.dataset.current) {
      return false;
    } else {
      that.setData({
        currentTab: e.target.dataset.current
      })
    }
  },

  bindDayChange: function (e) {
    var that = this;
    that.setData({
      selectDay: e.detail.value
    })
    utils.getBill(this, (res) => {
      this.setData({
        items: res.data.objects,
        day: res.data.objects
      })
      this.calTotalDay(this.data.items);
    })
  },

  bindMonthChange: function (e) {
    var that = this;
    that.setData({
      selectMonth: e.detail.value
    })
    utils.getBill(this, (res) => {
      this.setData({
        items: res.data.objects,
      })
      this.calTotalMonth(this.data.items);
      this.choiceMonth(this.data.items);
    })
  },

  bindYearChange: function (e) {
    var that = this;
    that.setData({
      selectYear: e.detail.value
    })
    utils.getBill(this, (res) => {
      this.setData({
        items: res.data.objects,
      })
      this.calTotalYear(this.data.items);
      this.choiceYear(this.data.items);
    })
  },

  calTotalDay: function (data) {
    var tempTotal1 = 0; //存放支出总金额
    var tempTotal2 = 0; //存放收入总金额
    var subtime = this.data.selectDay;
    //console.log(subtime);
    for (var x in data) {
      if (data[x].billCate == '-') {
        //如果是支出，则判断支出日期是否匹配选择日期
        if (subtime == data[x].billDate) {
          tempTotal1 += parseFloat(data[x].billMoney);
        }
      }
      if (data[x].billCate == '+') {
        //如果是收入，则判断收入日期是否匹配选择日期
        if (subtime == data[x].billDate) {
          tempTotal2 += parseFloat(data[x].billMoney);
        }
      }
    }
    this.setData({
      expenseTotal: tempTotal1,
      incomeTotal: tempTotal2
    });
  },

  calTotalMonth: function (data) {
    var tempTotal1 = 0; //存放支出总金额
    var tempTotal2 = 0; //存放收入总金额
    var subtime = this.data.selectMonth;
   // console.log(subtime);
    for (var x in data) {
      if (data[x].billCate == '-') {
        //如果是支出，则判断支出日期是否匹配选择月份
        if (subtime == data[x].billDate.substring(0, 7)) {
          tempTotal1 += parseFloat(data[x].billMoney);
        }
      }
      if (data[x].billCate == '+') {
        //如果是收入，则判断收入日期是否匹配选择月份
        if (subtime == data[x].billDate.substring(0, 7)) {
          tempTotal2 += parseFloat(data[x].billMoney);
        }
      }
    }
    this.setData({
      expenseTotal: tempTotal1,
      incomeTotal: tempTotal2
    });
  },

  calTotalYear: function (data) {
    var tempTotal1 = 0; //存放支出总金额
    var tempTotal2 = 0; //存放收入总金额
    var subtime = this.data.selectYear;
    //console.log(subtime);
    for (var x in data) {
      if (data[x].billCate == '-') {
        //如果是支出，则判断支出日期是否匹配选择年份
        if (subtime == data[x].billDate.substring(0, 4)) {
          tempTotal1 += parseFloat(data[x].billMoney);
        }
      }
      if (data[x].billCate == '+') {
        //如果是收入，则判断收入日期是否匹配选择年份
        if (subtime == data[x].billDate.substring(0, 4)) {
          tempTotal2 += parseFloat(data[x].billMoney);
        }
      }
    }
    this.setData({
      expenseTotal: tempTotal1,
      incomeTotal: tempTotal2
    });
  },

  choiceMonth: function (data) {
    let that = this;
    var x;
    var subtime = this.data.selectMonth;
    for (x = 0; x < data.length; x++) {
      if (subtime != data[x].billDate.substring(0, 7)) {
        data.splice(x, 1);
        x = x - 1;
      }
    }
    this.setData({
      month: data
    })
    //console.log(this.data.month);
  },

  choiceYear: function (data) {
    let that = this;
    var x;
    var subtime = this.data.selectYear;
    for (x = 0; x < data.length; x++) {
      if (subtime != data[x].billDate.substring(0, 4)) {
        data.splice(x, 1);
        x = x - 1;
      }
    }
    this.setData({
      year: data
    })
    //console.log(this.data.year);
  },

  onShareAppMessage: function () {
    return {
      title: '账单分享',
      path: '/pages/index/index',
      success: function (res) {
        //console.log(res)
      }
    }
  },
  onPullDownRefresh: function () {
    wx.stopPullDownRefresh()
  },
  imageLoad: function (e) {
    var imageSize = imageUtil.imageUtil(e)
    this.setData({
      imagewidth: imageSize.imageWidth,
      imageheight: imageSize.imageHeight
    })
  }
})