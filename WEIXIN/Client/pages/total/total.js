var util = require('../../utils/util.js')
var imageUtil = require('../../utils/imageutil.js');
import utils from '../../utils/index'
var app = getApp()
var choice = 0
var bottomheight = 0
Page({
  data: {
    items: [],
    tableID: app.globalData.tableId,
    selectMonth: util.formatTime(new Date).substring(0, 7),
    nowdate: util.formatTime(new Date),
    monthData: [],

    imagetotal1: 'https://gitee.com/git_ty1213/jizhang_program/raw/141fc6f193b2c19059e609e605c4c783e536bb08/WEIXIN/Client/images/total_1.png',

    imagetotal2: 'https://gitee.com/git_ty1213/jizhang_program/raw/feef383794d12c3ced60261c0cd907d93745b546/WEIXIN/Client/images/total2.png',

    imagetotal3: 'https://gitee.com/git_ty1213/jizhang_program/raw/feef383794d12c3ced60261c0cd907d93745b546/WEIXIN/Client/images/total3.jpg',


    imagedateBtn: 'https://gitee.com/git_ty1213/jizhang_program/raw/f9616d0ee4aee0efc70344daae35fc14fc579b06/WEIXIN/Client/images/dateBtn.jpeg'


  },
  onShow: function (e) {
    if (choice == 0) {
      utils.getBills(this, (res) => {
        this.setData({
          items: res.data.objects
        })
        this.billCanvas(this.data.items);
      });
    }
  },
  bindMonthChange: function (e) {
    var that = this;
    that.setData({
      selectMonth: e.detail.value
    })
    utils.getBill(this, (res) => {
      this.setData({
        items: res.data.objects,
      })
      this.choiceMonth(this.data.items);
    })
    if (this.data.selectMonth != util.formatTime(new Date).substring(0, 7)) {
      choice = 1;
    }

  },

  choiceMonth: function (data) {
    let that = this;
    var subtime = this.data.selectMonth;
    for (var x = 0; x < data.length; x++) {
      if (subtime != data[x].billDate.substring(0, 7)) {
        data.splice(x, 1);
        x = x - 1;
      }
    }
    this.setData({
      monthData: data
    })
    this.billCanvas(this.data.monthData);
   // console.log(this.data.monthData);
  },

  billCanvas: function (data) {
    var context = wx.createCanvasContext('Canvas');

    //支出的饼状图
    var expenseName = [];
    var expenseType = ["饮食", "服饰", "水电", "住房", "出行", "医疗", "娱乐", "其他"];
    var expenseArray = [];

    for (var x = 0; x < data.length; x++) {
      if (data[x].billCate == '-') {
        if (expenseName.length != 0) {
          for (var i = 0; i < expenseName.length; i++) {
            if (expenseName[i] == data[x].billName) {
              break;
            }
            else if (i == (expenseName.length - 1)) {
              expenseName[expenseName.length] = data[x].billName;
            }
          }
        }
        else {
          expenseName[0] = data[x].billName;
        }
      }
    }

    for (var i = 0; i < expenseName.length; i++) {
      expenseArray[i] = 0;
    }

    for (var x = 0; x < data.length; x++) {
      if (data[x].billCate == '-') {
        for (var i = 0; i < expenseName.length; i++) {
          if (expenseName[i] == data[x].billName) {
            expenseArray[i] += parseFloat(data[x].billMoney);
          }
        }
      }
    }

    var colors = ["#D8BFD8", "#FFC0CB", "#DDA0DD", "#EE82EE", "#DA70D6", "#BA55D3", "#DB7093", "#FF69B4"];
    var expenseTotal = 0;
    for (var val = 0; val < expenseArray.length; val++) {
      expenseTotal += expenseArray[val];
    }

    var expensePoint = { x: 105, y: 120 };
    var expensePos = { x: 195, y: 45 };
    var expenseWidth = 25, expenseHeight = 10;
    var expenseText = { x: 230, y: 55 };
    var expenseText2 = { x: 110, y: 20 };
    var radius = 80;

    for (var i = 0; i < expenseArray.length; i++) {
      context.beginPath();
      var start = 0;
      if (i > 0) {
        for (var j = 0; j < i; j++) {
          start += expenseArray[j] / expenseTotal * 2 * Math.PI;
        }
      }

      //饼状图
      context.arc(expensePoint.x, expensePoint.y, radius, start, start + expenseArray[i] / expenseTotal * 2 * Math.PI, false);
      context.setLineWidth(2)
      context.lineTo(expensePoint.x, expensePoint.y);
      context.setStrokeStyle('#F5F5F5');
      context.setFillStyle(colors[i]);
      context.fill();
      context.closePath();
      context.stroke();

      //图例
      context.fillStyle = colors[i];
      context.fillRect(expensePos.x, expensePos.y + 20 * i, expenseWidth, expenseHeight);
      context.moveTo(expensePos.x, expensePos.y + 20 * i);

      //图例说明
      context.font = 'bold 13px 微软雅黑';
      context.fillStyle = colors[i];
      context.fillText(expenseName[i] + ":" + expenseArray[i] + "元", expenseText.x, expenseText.y + 20 * i);

      //提示
      context.font = "25rpx Aria";
      context.fillStyle = "#EE82EE";
      context.fillText("支出统计", expenseText2.x, expenseText2.y)
    }

    //收入的饼状图
    var incomeName = [];
    var incomeType = ["奖金", "红包", "工资", "贷款", "理财", "礼金", "兼职", "其他"];
    var incomeArray = [];

    for (var x = 0; x < data.length; x++) {
      if (data[x].billCate == '+') {
        if (incomeName.length != 0) {
          for (var i = 0; i < incomeName.length; i++) {
            if (incomeName[i] == data[x].billName) {
              break;
            }
            else if (i == (incomeName.length - 1)) {
              incomeName[incomeName.length] = data[x].billName;
            }
          }
        //  console.log(incomeName);
        }
        else {
          incomeName[0] = data[x].billName;
          //console.log(incomeName);
        }
      }
    }

    for (var i = 0; i < incomeName.length; i++) {
      incomeArray[i] = 0;
    }

    for (var x = 0; x < data.length; x++) {
      if (data[x].billCate == '+') {
        for (var i = 0; i < incomeName.length; i++) {
          if (incomeName[i] == data[x].billName) {
            incomeArray[i] += parseFloat(data[x].billMoney);
          }
        }
      }
    }

    var incomeTotal = 0;

    for (var val = 0; val < incomeArray.length; val++) {
      incomeTotal += incomeArray[val];
    }

    var incomePoint = { x: 105, y: 335 };
    var incomePos = { x: 195, y: 265 };
    var incomeWidth = 25, incomeHeight = 10;
    var incomeText = { x: 230, y: 275 }
    var incomeText2 = { x: 110, y: 245 };
    var radius = 80;
    for (var i = 0; i < incomeArray.length; i++) {
      context.beginPath();
      var start = 0;
      if (i > 0) {
        for (var j = 0; j < i; j++) {
          start += incomeArray[j] / incomeTotal * 2 * Math.PI;
        }
      }
      //饼状图
      context.arc(incomePoint.x, incomePoint.y, radius, start, start + incomeArray[i] / incomeTotal * 2 * Math.PI, false);
      context.setLineWidth(2)
      context.lineTo(incomePoint.x, incomePoint.y);
      context.setStrokeStyle('#F5F5F5');
      context.setFillStyle(colors[i]);
      context.fill();
      context.closePath();
      context.stroke();

      //图例
      context.fillStyle = colors[i];
      context.fillRect(incomePos.x, incomePos.y + 20 * i, incomeWidth, incomeHeight);
      context.moveTo(incomePos.x, incomePos.y + 20 * i);

      //图例说明
      context.font = 'bold 13px 微软雅黑';
      context.fillStyle = colors[i];
      context.fillText(incomeName[i] + ":" + incomeArray[i] + "元", incomeText.x, incomeText.y + 20 * i);

      //提示
      context.font = "25rpx Aria";
      context.fillStyle = "#EE82EE";
      context.fillText("收入统计", incomeText2.x, incomeText2.y)
    }
    context.draw();
  },

  saveBillCanvas: function () {
    var context = wx.createCanvasContext('Canvas');
    context.draw(true, setTimeout(function () {
      wx.canvasToTempFilePath({
        canvasId: 'Canvas',
        quality: 1,
        success: function (res) {
          wx.saveImageToPhotosAlbum({
            filePath: res.tempFilePath,
          })
        }
      })
    }, 100))
  },
  onPullDownRefresh: function () {
    wx.stopPullDownRefresh()
  },
  imageLoad: function (e) {
    var imageSize = imageUtil.imageUtil(e)
    this.setData({
      imagewidth: imageSize.imageWidth,
      imageheight: imageSize.imageHeight
    })
  }

})