var util = require('../../utils/util.js');
var imageUtil = require('../../utils/imageutil.js');
import utils from '../../utils/index'
Page({

  /**
   * 页面的初始数据
   */
  data: {

    currentTab: '',
    nowdate: util.formatTime(new Date),

    cate: '',//收入为+，支出为-

    selectType_expense: true,
    firstType_expense: '',
    selectArea_expense: false,
    money_expense: '',
    selectdate_expense: util.formatTime(new Date),
    remarks_expense: '',

    selectType_income: true,
    firstType_income: '',
    selectArea_income: false,
    money_income: '',
    selectdate_income: util.formatTime(new Date),
    remarks_income: '',

    modalHidden_expense: true,
    alertHidden_expense: true,
    modalHidden_income: true,
    alertHidden_income: true,
    alertTitle: '',

    imagefirstsrc: 'https://gitee.com/git_ty1213/jizhang_program/raw/953c758bd61c42a287cbba5dd2734324b346f44b/WEIXIN/Client/images/expense.jpeg',

    imagetype1: 'https://gitee.com/git_ty1213/jizhang_program/raw/d16d9dadd8d72a1a99bd0dda9c853c3b2bfec344/WEIXIN/Client/images/type1.png',
    imagetype2: 'https://gitee.com/git_ty1213/jizhang_program/raw/d16d9dadd8d72a1a99bd0dda9c853c3b2bfec344/WEIXIN/Client/images/type2.png',
    imagetype3: 'https://gitee.com/git_ty1213/jizhang_program/raw/d16d9dadd8d72a1a99bd0dda9c853c3b2bfec344/WEIXIN/Client/images/type3.png',
    imagesavebtn:'https://gitee.com/git_ty1213/jizhang_program/raw/d16d9dadd8d72a1a99bd0dda9c853c3b2bfec344/WEIXIN/Client/images/savebtn.png',
  },
  bindChange: function (e) {
    var that = this;
    that.setData({ currentTab: e.detail.current });
  },
  /*** 点击tab切换***/
  swichNav: function (e) {
    var that = this;
    that.setData({
      currentTab: e.target.dataset.current
    });
  },
  //支出界面
  clickType_expense: function () {
    var that=this;
    var selectType_expense = this.data.selectType_expense;
    if (selectType_expense == true) {
      this.setData({
        selectArea_expense: true,
        selectType_expense: false,
      })
    } else {
      this.setData({
        selectArea_expense: false,
        selectType_expense: true,
      })
    }
  },
  //点击切换
  mySelect_expense: function (e) {
    this.setData({
      firstType_expense: e.currentTarget.dataset.mtype,
      selectType_expense: true,
      selectArea_expense: false,
    })
  },
  moneyInput_expense: function (e) {
    this.setData({
      money_expense: e.detail.value,
      cate: '-'
    })
  },
  bindDateChange_expense: function (e) {
    var that = this;
    that.setData({
      selectdate_expense: e.detail.value
    })
    //console.log(that.data.selectdate_expense);
  },
  remarksInput_expense: function (e) {
    this.setData({
      remarks_expense: e.detail.value
    })
  },
  saveBtnClick_expense: function (e) {
    let that = this;
    if (!this.data.firstType_expense) {
      that.setData({
        alertHidden_expense: false,
        alertTitle: '请选择类别！'
      });
      return;
    }

    let reg = /^[0-9]+.?[0-9]*$/;
    if (!reg.test(this.data.money_expense)) {
      that.setData({
        alertHidden_expense: false,
        alertTitle: '请输入金额且只能是数字'
      });
      return;
    }

    utils.addBill(this, (res) => {
      this.setData({
        modalHidden_expense: false,
        remarks_expense: '',
        firstType_expense: '',
        money_expense: '',
        selectdate_expense: util.formatTime(new Date),
      })
    });
  
  },
  //收入界面
  clickType_income: function () {
    var selectType_income = this.data.selectType_income;
    if (selectType_income == true) {
      this.setData({
        selectArea_income: true,
        selectType_income: false,
      })
    } else {
      this.setData({
        selectArea_income: false,
        selectType_income: true,
      })
    }
  },
  //点击切换
  mySelect_income: function (e) {
    this.setData({
      firstType_income: e.currentTarget.dataset.mtype,
      selectType_income: true,
      selectArea_income: false,
    })
  },
  moneyInput_income: function (e) {
    this.setData({
      money_income: e.detail.value,
      cate: '+'
    })
  },
  bindDateChange_income: function (e) {
    var that = this;
    that.setData({
      selectdate_income: e.detail.value
    })
    //console.log(that.data.selectdate_income);
  },
  remarksInput_income: function (e) {
    this.setData({
      remarks_income: e.detail.value
    })
  },
  saveBtnClick_income: function () {
    let that = this;
    if (!this.data.firstType_income) {
      that.setData({
        alertHidden_income: false,
        alertTitle: '请选择类别！'
      });
      return;
    }

    let reg = /^[0-9]+.?[0-9]*$/;
    if (!reg.test(this.data.money_income)) {
      that.setData({
        alertHidden_income: false,
        alertTitle: '请输入金额且只能是数字'
      });
      return;
    }

    utils.addBill(this, (res) => {
      this.setData({
        modalHidden_income: false,
        remarks_income: '',
        firstType_income: '',
        money_income: '',
        selectdate_expense: util.formatTime(new Date),
      })
    });
   
  },

  hideModal_expense: function () {
    this.setData({
      'modalHidden_expense': true,
      firstType_expense: '',
      selectdate_expense: util.formatTime(new Date),
      value_info: '',
      alertHidden_expense: true
    });
    wx.switchTab({
      url: '../index/index',
    });
  },

  hideModal_income: function () {
    this.setData({
      'modalHidden_income': true,
      firstType_income: '',
      selectdate_income: util.formatTime(new Date),
      value_info: '',
      alertHidden_income: true
    });
    wx.switchTab({
      url: '../index/index',
    });
  },

  hideAlertView_expense: function () {
    this.setData({
      alertHidden_expense: true
    })
  },

  hideAlertView_income: function () {
    this.setData({
      alertHidden_income: true
    })
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {

  },
  onPullDownRefresh:function(){
    wx.stopPullDownRefresh()
  },
  imageLoad: function (e) {
    var imageSize = imageUtil.imageUtil(e)
    this.setData({
      imagewidth: imageSize.imageWidth,
      imageheight: imageSize.imageHeight
    })
  }
})