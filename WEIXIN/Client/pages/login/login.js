//login.js
//获取应用实例
const app = getApp()
import utils from '../../utils/index'
var imageUtil = require('../../utils/imageutil.js');
Page({
  data: {
    motto: '欢迎使用EASY记！*_*',
    userInfo: {},
    hasUserInfo: false,
    canIUse: wx.canIUse('button.open-type.getUserInfo'),
    imagefirstsrc: 'https://gitee.com/git_ty1213/jizhang_program/raw/45ca4349ccfab9a21b1e789bfdddbf5d1c125cc7/WEIXIN/Client/images/login.jpeg',
    openid: '',
    primarySize: 'default',
    warnSize: 'default',
    disabled: false,
    plain: true,
    loading: false
  },

  getUserInfo: function (e) {
    if (true) {
      wx.BaaS.login()
        .then(res => {
          app.globalData.openid = wx.BaaS.storage.get('openid')
          this.setData({
            openid: app.globalData.openid
          })
        }).catch(err => {
          console.dir(err)
        })
    }
    var that = this;
    app.globalData.userInfo = e.detail.userInfo
    that.setData({
      userInfo: e.detail.userInfo,
      hasUserInfo: true,
    })
    setTimeout(function(){
      wx.switchTab({
        url: '../index/index',
      })
    },100);
  },

  imageLoad: function (e) {
    var imageSize = imageUtil.imageUtil(e)
    this.setData({
      imagewidth: imageSize.imageWidth,
      imageheight: imageSize.imageHeight
    })
  }
})