//app.js

App({
  onLaunch: function () {
    let that = this

    // 引入 BaaS SDK
    require('./utils/sdk-v1.4.0')


    let clientId = this.globalData.clientId

    wx.BaaS.init(clientId)

  },


  globalData: {
    openid: null,
    userInfo: null,
    clientId: '0bc279fa4a7a7233596e', // 从 BaaS 后台获取 ClientID
    tableId: 37718, //37742, //37383, // 从 https://cloud.minapp.com/dashboard/ 管理后台的数据表中获取 
    tableId_b: 38476
  }
})

//https://gitee.com/git_ty1213/jizhang_program.git
