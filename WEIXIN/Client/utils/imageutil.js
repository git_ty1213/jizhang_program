function imageUtil(e) {
  var imageSize = {};
  var originalWidth = e.detail.width;//图片原始宽  
  var originalHeight = e.detail.height;//图片原始高  
  //console.log('originalWidth: ' + originalWidth)
  //console.log('originalHeight: ' + originalHeight)
  //获取屏幕宽高  
  wx.getSystemInfo({
    success: function (res) {
      var windowWidth = res.windowWidth;
      var windowHeight = res.windowHeight;
     // console.log('windowWidth: ' + windowWidth)
     // console.log('windowHeight: ' + windowHeight)

      imageSize.imageWidth = windowWidth;
      imageSize.imageHeight = windowHeight;

    }
  })
  console.log('缩放后的宽: ' + imageSize.imageWidth)
  console.log('缩放后的高: ' + imageSize.imageHeight)
  return imageSize;
}

module.exports = {
  imageUtil: imageUtil
}